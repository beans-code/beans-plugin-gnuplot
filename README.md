# Gnuplot plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Gnuplot` is a portable graphing utility which allows easily prepare scientific quality plots [http://www.gnuplot.info](http://www.gnuplot.info).

## Features

`BEANS` supports `Gnuplot` scripts and the main features are:

* in `Gnuplot plugin` scripts one can use column names, not the number of columns. This makes the `Gnuplot` scripts much easier to read for others. 
* `Gnuplot` scripts can directly read data from multiple different BEANS datasets and tables
* `Gnuplot` scripts support `splitby` and/or `colorby` statements described below, which are greate for efficient data analysis
* all features of `Gnuplot` are available in this plugin too

## Example

This example `Gnuplot` script plots total cluster mass a dataset(s) found with query `mocca` and from table(s) found with query `system`:

```gnuplot
plot 'datasets="mocca" tables="system"' using ($tphys):($smt) title 'total mass';
```

The main advantage of using `Gnuplot` scripts in `BEANS` is that you can use column names (not the column indices) and you can read in parallel many different datasets and tables from within `BEANS`. If there are multiple datasets and tables which are found with these queries then all of them will be plotted on the same figure. 

### splitby

The keyword `splitby` can split one `Gnuplot` script by a column and producing a series of plots.

```gnuplot
plot 'datasets="mocca" tables="system" splitby="tbid"' using ($tphys):($smt) title 'total mass';
```

This `Gnuplot` script will divide the read tables into separate plots based on the values of column `tbid`. If there are e.g. 5 different values for the column `tbid`, then 5 separate plots will be created - one for every plot. 

### colorby

The keyword `colorby` work similarly to `splitby` with the only difference that instead of producing separate plots it produces all of them on one Figure but data are colored differently based on the `colorby` values.

```gnuplot
plot 'datasets="mocca" tables="system" colorby="tbid"' using ($tphys):($smt) title 'total mass';
```

This `Gnuplot` script will divide the data read from tables into separate datasets based on the values of column `tbid`. If there are e.g. 5 different values for the column `tbid`, then 5 separate lines/points will be plotted on one figure but with different colors.