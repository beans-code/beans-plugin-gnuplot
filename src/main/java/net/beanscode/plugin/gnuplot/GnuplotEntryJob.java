package net.beanscode.plugin.gnuplot;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class GnuplotEntryJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID gnuplotEntryId = null;
	
	private GnuplotEntry gnuplotEntry = null;

	public GnuplotEntryJob() {
		
	}
	
	public GnuplotEntryJob(GnuplotEntry ge) {
		setGnuplotEntryId(ge.getId());
	}
	
	@Override
	protected String getLogSeparately() {
//		try {
			// TODO make logs more general to work with any notebook entry - it will be useful for actually any plugin
//			FileExt f = new FileExt(getGnuplotEntry().getGnuplotWrapper().getWorkingFolderPath() 
//					+ "/gnuplot-" + getGnuplotEntryId() + ".log");
//			return f.getAbsolutePath();
			return new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/gnuplot/",
					"/gnuplot-" + getGnuplotEntryId() + ".log")
				.getAbsolutePath();
//		} catch (IOException e) {
//			LibsLogger.error(PigScriptJob.class, "Cannot log separatelly Job", e);
//			return super.getLogSeparately();
//		}
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.info(GnuplotEntryJob.class, "Starting gnuplot entry wrapper");
		
		getGnuplotEntry().reload(false);
		
		if (getGnuplotEntry() != null
				&& getGnuplotEntry().getGnuplotWrapper() != null)
			for (FileExt f : getGnuplotEntry().getGnuplotWrapper().iterateOutputFiles())
				LibsLogger.info(GnuplotEntryJob.class, "Output: " + f.getAbsolutePath());
		
		LibsLogger.info(GnuplotEntryJob.class, "Finished gnuplot entry wrapper");
	}
	
	public GnuplotEntry getGnuplotEntry() throws IOException {
		if (gnuplotEntry == null)
			gnuplotEntry = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(getGnuplotEntryId());
		return gnuplotEntry;
	}

	public UUID getGnuplotEntryId() {
		return gnuplotEntryId;
	}

	public void setGnuplotEntryId(UUID gnuplotEntryId) {
		this.gnuplotEntryId = gnuplotEntryId;
	}
}
