package net.beanscode.plugin.gnuplot;

import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;


public class GnuplotEntryClearJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private String gnuplotJson = null;
	
	private GnuplotEntry gnuplotEntryCache = null;

	public GnuplotEntryClearJob() {
		
	}
	
	public GnuplotEntryClearJob(GnuplotEntry ge) {
		setGnuplotJson(JsonUtils.objectToString(ge));
		setGnuplotEntryCache(ge);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getGnuplotEntryCache() != null
				&& getGnuplotEntryCache().getGnuplotWrapper() != null
				&& getGnuplotEntryCache().getGnuplotWrapper().getWorkingFolder() != null) {
			getGnuplotEntryCache().getGnuplotWrapper().getWorkingFolder().deleteIfExists();
			
			new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/gnuplot/",
					"/gnuplot-" + getGnuplotEntryCache().getId() + ".log")
				.deleteIfExists();
		}
	}

	private String getGnuplotJson() {
		return gnuplotJson;
	}

	private void setGnuplotJson(String gnuplotJson) {
		this.gnuplotJson = gnuplotJson;
	}

	private GnuplotEntry getGnuplotEntryCache() {
		if (gnuplotEntryCache == null) {
			gnuplotEntryCache = JsonUtils.fromJson(getGnuplotJson(), GnuplotEntry.class);
		}
		return gnuplotEntryCache;
	}

	private void setGnuplotEntryCache(GnuplotEntry gnuplotEntryCache) {
		this.gnuplotEntryCache = gnuplotEntryCache;
	}
}
