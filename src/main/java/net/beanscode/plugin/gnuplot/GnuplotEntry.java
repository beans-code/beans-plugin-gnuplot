package net.beanscode.plugin.gnuplot;

import static net.hypki.libs5.utils.string.RegexUtils.firstGroupCaseInsensitive;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.MetaUtils;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.model.utils.ApplyParams;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.plot.gnuplot.GnuplotWrapper;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.Tokenizer;

public class GnuplotEntry extends NotebookEntry {
	
	public static final String META_SCRIPT 		= "script";
	public static final String META_WRAPPER		= "wrapper";
	public static final String META_STATUS		= "status";
	public static final String META_COMPILEDSCRIPT 		= "compiledscript";

//	private String gnuplotScript = null;

	private GnuplotWrapper gnuplotWrapper = null;
	
//	private Map<String, String> id2query = null;
	private Map<String, String> query2id = null;
	private TripleMap<String, String, Table> datasetTableToDef = null;
	
	public GnuplotEntry() {
		
	}

	@Override
	public String getShortDescription() {
		return "Gnuplot";
	}

	@Override
	public String getSummary() {
		return "Gnuplot entry allows to create a plot using gnuplot pure scripts and some help from BEANS to "
				+ "create scientific ready plots";
	}

	@Override
	public boolean isReloadNeeded() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
	}

	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		reload(true);
		
		return ReloadPropagator.NONBLOCK;
	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		setGnuplotWrapper(getGnuplotWrapper());
		
		return super.save();
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new GnuplotEntryClearJob(this));
	}
	
	public void reload(boolean inBackground) throws ValidationException, IOException {
		if (inBackground) {
			getProgress()
				.running(0.0, "Running...")
				.save();
			new GnuplotEntryJob(this).save();
			return;
		}
		
		try {
			// clear previous
			setGnuplotScript(getGnuplotScript());
			getProgress()
				.running(0.0, "Running...")
				.save();
			
			// status: started
			save();
			
			// clear tmp data structures
			getQuery2Id().clear();
			
			// create working directory
			FileExt workingDir = new FileExt(PluginManager.getPluginDefaultFolder(), "/gnuplot/", getId().getId());
			workingDir.mkdirs();
			workingDir.cleanDir();
			
			// compiling gnuplot script and creating input files
			compileGnuplotScript();
	
			// converting some potential BEANS queries or columns names in gnuplot script to pure gnuplot
			setGnuplotWrapper(new GnuplotWrapper(getMetaAsString(META_COMPILEDSCRIPT))
								.setGnuplotFilename("gnuplot-wrapper-" + getId() + ".gpl")
								.setWorkingFolderPath(workingDir.getAbsolutePath()));
			
			LibsLogger.info(GnuplotEntry.class, "Original gnuplot: \n" + getGnuplotScript());
			LibsLogger.info(GnuplotEntry.class, "Compiled BEANS gnuplot: \n" + getGnuplotWrapper().getGnuplotScript());
			
			// run actuall gnuplot
			getGnuplotWrapper()
				.run();
			
			int i = 0;
			for (FileExt output : getGnuplotWrapper().iterateOutputFiles()) {
				setMeta("file-name-" + (i++), output.getAbsolutePath());
			}
			
			// status: finished
			save();
			
			getProgress()
				.ok()
				.save();
			LibsLogger.info(GnuplotEntry.class, "Progress " + getProgress());
		} catch (Throwable t) {
			LibsLogger.error(GnuplotEntry.class, "Gnuplot failed", t);
			getProgress()
				.fail("Gnuplot failed: " + t.getMessage())
				.save();
		}
	}
	
	private ColumnDefList getColumnDefList(String dsQ, String tbQ) {
		if (datasetTableToDef.get(dsQ, tbQ) != null) {
			return datasetTableToDef.get(dsQ, tbQ).getColumnDefs();
		} else {
			for (Table table : TableFactory.iterateTables(getUserId(), dsQ, tbQ)) {
				if (table.getColumnDefs() != null) {
					ColumnDefList defs = table.getColumnDefs();
					datasetTableToDef.put(dsQ != null ? dsQ : "", 
							tbQ != null ? tbQ : "", 
							table);
					return defs;
				}
			}
		}
		return null;
	}
	
	private void compileGnuplotScript() throws IOException {
		String splitName = null;
		List<Object> splitsValues = new ArrayList<>();
		
		// checking if there are splits
		String line = null;
		boolean append = false;
		for (String l : StringUtilities.split(getGnuplotScript(), '\n')) {
			// check for multilines in gnuplot script
			if (l.trim().endsWith("\\")) {
				line += l.substring(0, l.length() - 2) + " ";
				append = true;
				continue;
			} else {
				if (append) {
					append = false;
					line += l;
				} else {
					line = l;
				}
			}
			
			if ((line.contains("plot ") || line.contains("splot ") || line.contains("fit "))
					&& (RegexUtils.firstGroupCaseInsensitive("(datasets[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null
						|| RegexUtils.firstGroupCaseInsensitive("(tables[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null)) {
				Tokenizer t = new Tokenizer(line);
				
				while (true) {
					// parsing input data
					String dsQ = t.pullOut("datasets[\\s]*=[\\s]*\"([^\\\"]+)\"");
					dsQ = RegexUtils.firstGroup("\"(.*)\"", dsQ);
					dsQ = MetaUtils.applyMeta(dsQ, getNotebook());
					
					String tbQ = t.pullOut("tables[\\s]*=[\\s]*\"([^\\\"]+)\"");
					tbQ = RegexUtils.firstGroup("\"(.*)\"", tbQ);
					tbQ = MetaUtils.applyMeta(tbQ, getNotebook());
					
					String splitByTerm = t.pullOut("splitby[\\s]*=[\\s]*\"([^\\\"]+)\"");
					splitByTerm = RegexUtils.firstGroup("\"(.*)\"", splitByTerm);
					boolean isSplitBy = notEmpty(splitByTerm);
					
					String colorByTerm = t.pullOut("colorby[\\s]*=[\\s]*\"([^\\\"]+)\"");
					colorByTerm = RegexUtils.firstGroup("\"(.*)\"", colorByTerm);
					boolean isColorBy = notEmpty(colorByTerm);
					Set<Object> colorByValues = new HashSet<Object>();
					
					String filterTerm = t.pullOut("filter[\\s]*=[\\s]*\"([^\\\"]+)\"");
					filterTerm = RegexUtils.firstGroup("\"(.*)\"", filterTerm);
					boolean isFiltered = notEmpty(filterTerm);
					
					// preparing input data
					if (isSplitBy) {
						splitName = splitByTerm;
						for (Table table : TableFactory.iterateTables(getUserId(), dsQ, tbQ)) {
							for (Row row : table.iterateRows(isFiltered ? Query.parseQuery(filterTerm) : null)) {
								Object s = row.get(splitByTerm);
								if (!splitsValues.contains(s))
									splitsValues.add(s);
							}
						}
					}
					break;
				}
				
				if (splitName != null)
					break;
			}
		}
		
		StringBuilder allSplits = new StringBuilder();
		if (splitName != null) {
			CollectionUtils.autoSort(splitsValues);
			
			for (Object oneSplitBy : splitsValues) {
				String compiledOneScript = compileGnuplotScript(splitName, oneSplitBy);
				
				// fixing the filename, so it will not be replaced by the next split
//				compiledOneScript.replaceFirst(splitName, line)
				
				allSplits.append(compiledOneScript);
				
				allSplits.append("\n\nreset;");
			}
		} else {
			allSplits.append(compileGnuplotScript(splitName, null));
		}
		setMeta(META_COMPILEDSCRIPT, allSplits.toString());
	}
	
	private String compileGnuplotScript(String splitByName, Object splitBy) throws IOException {
		datasetTableToDef = new TripleMap<>();
		StringBuilder oneSplit = new StringBuilder();
		FileExt workingDir = new FileExt(PluginManager.getPluginDefaultFolder(), "/gnuplot/", getId().getId());
		
		boolean append = false;
		String line = "";
		String setTitle = null;
		boolean setTitleCompiled = true;
		String dsQPrev = null;
		String tbQPrev = null;
		String colorByPrev = null;
		for (String l : StringUtilities.split(getGnuplotScript(), '\n')) {
			// check for multilines in gnuplot script
			if (l.trim().endsWith("\\")) {
				line += l.substring(0, l.length() - 2) + " ";
				append = true;
				continue;
			} else {
				if (append) {
					append = false;
					line += l;
				} else {
					line = l;
				}
			}
			
			if (RegexUtils.contains("set[\\s]+title[\\s]+", line)) {
				// saving set title '..' lines because there might be some params there
				/////////////////////////////
				setTitle = line;
				oneSplit.append(setTitle);
				oneSplit.append("\n");
				
				if (notEmpty(setTitle)
						&& RegexUtils.firstGroup("(\\$[\\w\\d\\_]+)", setTitle) != null) {
					setTitleCompiled = false;
				} else {
					setTitleCompiled = true;
				}
			} else if ((line.trim().startsWith("plot") || line.trim().startsWith("splot") || line.trim().startsWith("fit"))
					&& (RegexUtils.firstGroupCaseInsensitive("(datasets[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null
						|| RegexUtils.firstGroupCaseInsensitive("(tables[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null)) {
				// changing plot '..' lines
				/////////////////////////////
				
				Tokenizer t = new Tokenizer(line);
				
				// plot|splot|fit
				oneSplit.append(t.expectedMatchRegex("[\\s]*(plot|splot|fit)[\\s]+"));
				
				while (true) {
					// parsing input data
					String dsQ = t.pullOut("datasets[\\s]*=[\\s]*\"([^\\\"]+)\"");
					dsQ = RegexUtils.firstGroup("\"(.*)\"", dsQ);
					dsQ = MetaUtils.applyMeta(dsQ, getNotebook());
					if (nullOrEmpty(dsQ))
						dsQ = dsQPrev;
					dsQPrev = dsQ;
					
					String tbQ = t.pullOut("tables[\\s]*=[\\s]*\"([^\\\"]+)\"");
					tbQ = RegexUtils.firstGroup("\"(.*)\"", tbQ);
					tbQ = MetaUtils.applyMeta(tbQ, getNotebook());
					if (nullOrEmpty(tbQ))
						tbQ = tbQPrev;
					tbQPrev = tbQ;
					
					String splitByTerm = t.pullOut("splitby[\\s]*=[\\s]*\"([^\\\"]+)\"");
					splitByTerm = RegexUtils.firstGroup("\"(.*)\"", splitByTerm);
					
					String colorByTerm = t.pullOut("colorby[\\s]*=[\\s]*\"([^\\\"]+)\"");
					colorByTerm = RegexUtils.firstGroup("\"(.*)\"", colorByTerm);
					if (nullOrEmpty(colorByTerm))
						colorByTerm = colorByPrev;
					colorByPrev = colorByTerm;
					boolean isColorBy = notEmpty(colorByTerm);
					Set<Object> colorByValues = new HashSet<Object>();
					
					String sortByTerm = t.pullOut("sortby[\\s]*=[\\s]*\"([^\\\"]+)\"");
					sortByTerm = RegexUtils.firstGroup("\"(.*)\"", sortByTerm);
					boolean isSortBy = notEmpty(sortByTerm);
					
					String filterTerm = t.pullOut("filter[\\s]*=[\\s]*\"([^\\\"]+)\"");
					filterTerm = RegexUtils.firstGroup("\"(.*)\"", filterTerm);
					boolean isFiltered = notEmpty(filterTerm);
					
					String title = t.pullOut("title[\\s]+\'([^\\\']+)\'");
					title = RegexUtils.firstGroup("\'(.*)\'", title);
					boolean isTitle = notEmpty(title);
					
					t.expectedMatchRegex("[\\s]*[\\'\\\"]+[\\s]*[\\'\\\"]+[\\s]*u[sing]*[\\s]+");
					
					// using
					String using = t.contains(",[\\s]*(\\'|\\\")") ? 
							t.expectedMatchRegex("[^,]+,[\\s]*") : null;
					if (using == null) {
						using = t.getAll();
						t.clear();
					}
					using = using.trim();
					if (using.endsWith(";")
							|| using.endsWith(","))
						using = using.substring(0, using.length() - 1);
					
					// building column definitions
					ColumnDefList defs = null;
					for (Table table : TableFactory.iterateTables(getUserId(), dsQ, tbQ)) {
						if (defs == null) {
							defs = (ColumnDefList) table.getColumnDefs().clone();
							break;
						}
					}
					
					// converting columns names (e.g $x, $tphys) into specific columns indices (e.g. $2, $17)
					while (true) {
						String col = firstGroupCaseInsensitive("(\\$[^0-9][\\w\\d]*)[^\\w\\d]+", using);
						
						if (col == null)
							break;
						
						using = StringUtils.replaceOnce(using, col, "$" + (defs.getColumnIndex(col.substring(1)) + 1));
					}

					// convert any e.g. stringcolumn($19) to stringcolumn(19)
					////////////////////////////////////////////////////////////
					String prevLine = using;
					while (true) {
						String col = firstGroupCaseInsensitive("(stringcolumn\\(\\$[\\d]+\\))", using);
						
						if (col == null)
							break;
						
						using = using.replace(col, col.replace("$", ""));
						
						if (prevLine.equals(using))
							break;
						prevLine = using;
					}
					
					// color by values
					if (isColorBy) {
						for (Table table : TableFactory.iterateTables(getUserId(), dsQ, tbQ)) {
							for (Row row : table.iterateRows(isFiltered ? Query.parseQuery(filterTerm) : null)) {
								colorByValues.add(row.get(colorByTerm));
							}
						}
					} else {
						colorByValues.add(null);
					}
					
					// preparing input data
					boolean addSemi = false;
					for (Object colorByOneValue : colorByValues) {
						if (addSemi)
							oneSplit.append(", \\\n");
						
						String titleCompiled = null;
						
						final String inputFilename = "input-data-" + UUID.random().getId();
						PlainConnector plain = new PlainConnector(new FileExt(workingDir.getAbsolutePath(),
								inputFilename));
						for (Table table : TableFactory.iterateTables(getUserId(), dsQ, tbQ)) {
							if (plain.getColumnDefs() == null) {
								plain.setColumnDefs(defs);
							}
							for (Row row : table.iterateRows(isFiltered ? Query.parseQuery(filterTerm) : null)) {
								boolean rowOK = false;
								if (colorByOneValue == null) {
									rowOK = true;
								} else if (row.get(colorByTerm.toString()).equals(colorByOneValue)) {
									rowOK = true;
								}
								
								if (rowOK
										&& splitByName != null) {
									rowOK = row.get(splitByName).equals(splitBy);
								}
								
								if (rowOK) {
									plain.write(row);
									if (isTitle && titleCompiled == null)
										titleCompiled = ApplyParams.applyParams(title, row);
									
									if (setTitleCompiled == false) {
										setTitleCompiled = true;
										String tmp = ApplyParams.applyParams(setTitle, row);
										oneSplit = new StringBuilder(oneSplit.toString().replace(setTitle, tmp));
									}
								}
								
							}
						}
						plain.close();
						
						// writing compiled script
						oneSplit.append("'" + inputFilename + "'");
						oneSplit.append(" using ");
						oneSplit.append(using);
						if (isTitle)
							oneSplit.append(" title '" + titleCompiled + "'");
						
						addSemi = true;
					}
					
					if (t.isEmpty())
						break;
					
					oneSplit.append(", ");
				}
				
				// end of plot commands
				oneSplit.append(";\n");
				continue;
			}
			
			oneSplit.append(line);
			oneSplit.append("\n");
			
			line = "";
		}

		// adding default 'set output ...' (if needed)
		if (!RegexUtils.contains("set[\\s]+output[\\s]+", oneSplit.toString())) {
			String title = getName();
			if (nullOrEmpty(title))
				title = "no_name";
			title += (splitByName != null ? "-" + splitBy : "");
			title = title.replaceAll("[^\\w\\d]+", "_");
			title += ".png";
			oneSplit.insert(0, "set output '" + title + "';\n");
		} else {
			if (splitByName != null) {
				// adding default set output
				String title = getName();
				if (nullOrEmpty(title))
					title = "no_name.png";
				if (!notEmpty(new FileExt(title).getExtensionOnly()))
					title += ".png";
				title = new Tokenizer(title)
							.consumeLeft("set")
							.consumeLeft("output")
							.consumeLeft("'")
							.consumeLeft("\"")
							.consumeRight(";")
							.consumeRight("'")
							.consumeRight("\"")
							.replaceAll("[^\\w\\d\\.]+", "_")
							.appendBeforeRegex("\\.[\\w\\d]+", splitByName != null ? "-" + splitBy : "")
							.toString();
				
				// replacing old output
				Tokenizer newOneSplit = new Tokenizer(oneSplit.toString())
					.replaceAll("set[\\s]+output[\\s]+'.*'[\\s]*", 
							"set output '" + title + ".png';");
				oneSplit = new StringBuilder(newOneSplit.toString());
			}
		}
		
		// adding default 'set term ...' if needed
		if (!RegexUtils.contains("set[\\s]+term", oneSplit.toString())) {
			// adding default set term
			oneSplit.insert(0, "set term png;\n");
		}

		return oneSplit.toString();
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plots.GnuplotEntryEditorPanel";
	}

	@Override
	public void start() throws ValidationException, IOException {				
		getProgress()
			.running(0.0, "Running...")
			.save();
		
		setGnuplotScript(getGnuplotScript());
		
		save();
		
		reload();
	}

	@Override
	public boolean isRunning() throws ValidationException, IOException {
		return false;
	}

	@Override
	public void stop() throws ValidationException, IOException {
		// do nothing
	}

	public GnuplotWrapper getGnuplotWrapper() {
		if (this.gnuplotWrapper == null)
			this.gnuplotWrapper = getMetaAsObject(META_WRAPPER, GnuplotWrapper.class);
		return this.gnuplotWrapper;
	}

	public void setGnuplotWrapper(GnuplotWrapper gnuplotWrapper) {
		setMeta(META_WRAPPER, JsonUtils.objectToStringPretty(gnuplotWrapper));
		this.gnuplotWrapper = gnuplotWrapper;
	}

	public String getGnuplotScript() {
		return getMetaAsString(META_SCRIPT, null);
	}

	public void setGnuplotScript(String gnuplotScript) {
		getMeta().clear();
		
		setMeta(META_SCRIPT, gnuplotScript);
	}

//	private Map<String, String> getId2Query() {
//		if (id2query == null)
//			id2query = new HashMap<String, String>();
//		return id2query;
//	}
	
	private Map<String, String> getQuery2Id() {
		if (query2id == null)
			query2id = new HashMap<String, String>();
		return query2id;
	}
}
