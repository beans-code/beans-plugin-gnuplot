package net.beanscode.plugin.gnuplot;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

public class GnuplotInput {

	private UUID id = null;
	
	private String dsQuery = null;
	
	private String tbQuery = null;
	
	private String splitByTerm = null;
	
	private List<Object> splitByValues = null;
	
	private String colorByTerm = null;
	
	private List<Object> colorByValues = null;
	
	private String sortBy = null;
	
	public GnuplotInput() {
		
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getDsQuery() {
		return dsQuery;
	}

	public void setDsQuery(String dsQuery) {
		this.dsQuery = dsQuery;
	}

	public String getTbQuery() {
		return tbQuery;
	}

	public void setTbQuery(String tbQuery) {
		this.tbQuery = tbQuery;
	}

	public String getSplitByTerm() {
		return splitByTerm;
	}

	public void setSplitByTerm(String splitByTerm) {
		this.splitByTerm = splitByTerm;
	}

	public List<Object> getSplitByValues() {
		if (splitByValues == null)
			splitByValues = new ArrayList<>();
		return splitByValues;
	}

	public void setSplitByValues(List<Object> splitByValues) {
		this.splitByValues = splitByValues;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getColorByTerm() {
		return colorByTerm;
	}

	public void setColorByTerm(String colorByTerm) {
		this.colorByTerm = colorByTerm;
	}

	public List<Object> getColorByValues() {
		if (colorByValues == null)
			colorByValues = new ArrayList<>();
		return colorByValues;
	}

	public void setColorByValues(List<Object> colorByValues) {
		this.colorByValues = colorByValues;
	}
}
