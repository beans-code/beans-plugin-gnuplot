package net.beanscode.plugin.gnuplot;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.gnuplot.GnuplotEntry;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class GnuplotEntryUT extends BeansTestCase {

	public GnuplotEntryUT() {
		
	}
	
	@Test
	public void testSimpleGnuplot() throws ValidationException, IOException {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("set term pdf enhanced color; "
						+ "set output \"beans-gnuplot-wrapper-1.pdf\";  "
						+ "plot x; ");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
		
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		for (FileExt f : ge.getGnuplotWrapper().iterateOutputFiles()) {
			LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ", f.getAbsolutePath());
		}
		
	}
	
	@Test
	public void testGnuplotWithBeansTables() throws Exception {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		// creating test Table
		final Dataset dataset = createDataset(user, "test-ds");
		createTestTableWithData(dataset, "line hsuw652s");
		
		runAllJobs(1000);
		
		// run gnuplot
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("set term pdf enhanced color;\n"
						+ "set output \"beans-gnuplot-wrapper-2.pdf\"; \n"
						+ "plot 'datasets=\"test ds\" tables=\"line hsuw652s\"' u ($x):($y) w l title 'title '' escaped',"
						+ "			'' u ($1):($2+10.0);");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
		
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		for (FileExt f : ge.getGnuplotWrapper().iterateOutputFiles()) {
			LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ", f.getAbsolutePath());
		}
		
	}
	
	@Test
	public void testGnuplotHistogram() throws Exception {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		// creating test Table
		final Dataset dataset = createDataset(user, "test-ds");
		createTestTableWithData(dataset, "line hsuw652s");
		
		runAllJobs(1000);
		
		// run gnuplot
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("reset;\n" + 
				"set term pdf enhanced color;\n" + 
				"set output \"mass.pdf\";\n" + 
				"binwidth=1;\n" + 
				"bin(x,width)=width*floor(x/width);\n" +  
				"plot 'datasets=\"mocca\" tables=\"init e6542515\"' u (bin($sm1+$sm2,binwidth)):(1.0) smooth freq with boxes;");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
		
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		for (FileExt f : ge.getGnuplotWrapper().iterateOutputFiles()) {
			LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ", f.getAbsolutePath());
		}
		
	}
	
	@Test
	public void testGnuplotHistogram2() throws Exception {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		// creating test Table
		final Dataset dataset = createDataset(user, "test-ds");
		createTestTableWithData(dataset, "line hsuw652s");
		
		runAllJobs(1000);
		
		// run gnuplot
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("reset;\n" + 
				"set term pdf enhanced color;\n" + 
				"set output \"mass.pdf\";\n" + 
				"binwidth=1;\n" + 
				"bin(x,width)=width*floor(x/width);\n" +  
				"plot 'datasets=\"BH spins\" tables=\"b5ab0bde\"' u (bin($bhtype != \"IMBH\" ? $oldMass : NaN,binwidth)):(1.0) smooth freq with boxes;");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
		
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		for (FileExt f : ge.getGnuplotWrapper().iterateOutputFiles()) {
			LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ", f.getAbsolutePath());
		}
		
	}
	
	@Test
	public void testGnuplotHistogram3() throws Exception {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		// creating test Table
		final Dataset dataset = createDataset(user, "test-ds");
		createTestTableWithData(dataset, "line hsuw652s");
		
		runAllJobs(1000);
		
		// run gnuplot
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("reset;\n" + 
				"set term pdf enhanced color;\n" + 
				"set output \"mass.pdf\";\n" + 
				"binwidth=1;\n" + 
				"bin(x,width)=width*floor(x/width);\n" +  
				"plot 'datasets=\"BH spins\" tables=\"Final spins noIMBH max 20MSun bhtype prim\"' u (bin($oldMass,binwidth)):(1.0) smooth freq with boxes title \"Prim\"," + 
				"     'datasets=\"BH spins\" tables=\"Final spins noIMBH max 20MSun bhtype dyn\"' u (bin($oldMass,binwidth)):(1.0) smooth freq with boxes title \"Dyn\";");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
		
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		for (FileExt f : ge.getGnuplotWrapper().iterateOutputFiles()) {
			LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ", f.getAbsolutePath());
		}
		
	}
	
	@Test
	public void testGnuplotHistogram4() throws Exception {
		final User user = createTestUser("test");
		final Notebook notebook = createNotebook(user, "notebookName");
		
		// creating test Table
		final Dataset dataset = createDataset(user, "test-ds");
		createTestTableWithData(dataset, "line hsuw652s");
		
		runAllJobs(1000);
		
		// run gnuplot
		GnuplotEntry ge = new GnuplotEntry();
		ge
			.setGnuplotScript("reset;\n" + 
				"set term pdf enhanced color;\n" + 
				"set output \"mass.pdf\";\n" + 
				"binwidth=1;\n" + 
				"bin(x,width)=width*floor(x/width);\n" +  
				"plot 'datasets=\"BH spins\" tables=\"Final spins noIMBH max 20MSun\"' u (bin($oldMass,binwidth)):(1.0) smooth freq with boxes title \"Prim\"," + 
				"     'datasets=\"BH spins\" tables=\"Final spins noIMBH max 20MSun\"' u (bin($oldMass,binwidth)):(1.0) smooth freq with boxes title \"Dyn\";");
		ge.setUserId(user.getUserId());
		ge.setNotebookId(notebook.getId());
		ge.save();
				
		ge.reload();
		
		runAllJobs(1000);
		
		ge = (GnuplotEntry) NotebookEntryFactory.getNotebookEntry(ge.getId());
		LibsLogger.info(GnuplotEntryUT.class, "BEANS gnuplot wrapper output: ");
		System.out.println(ge.getGnuplotWrapper().getGnuplotScript());		
	}
}
